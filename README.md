# :dizzy: Demandez-moi n'importe quoi ! :sparkles:

:speech_balloon: [Posez une question](../../issues/new) | :book: [Lisez les questions](../../issues?q=is%3Aissue+is%3Aclosed+sort%3Aupdated-desc)

Je reçois parfois des questions par courriel. En la posant ici, tout le monde peut lire la réponse !

[![AMA](https://img.shields.io/badge/ask%20me-anything-1abc9c.svg)](https://bitbucket.org/lbesson/ama.fr)

N'importe quoi veut vraiment dire *n'importe quoi*.
Des questions personnelles. Argent. Travail. Vie. Code.
Cuisine. Voyage. Sports. Enseignement. Pokémon. N'importe quoi. :joy:

### :memo: Conseils

 - :mag: Vérifiez que votre question n'a pas déjà été posée.
 - :memo: Préférez un titre et une description rapide.
 - :bug: Les demandes de fonctionnalités additionnelles ou les notifications de bogues devraient être ouvertes sur le traqueur de bogues approprie (i.e., le traqueur de bogue correspondant au projet, sur [Bitbucket](https://bitbucket.org/lbesson/) ou [GitHub](https://github.com/Naereen/)).
 - :signal_strength: Des demandes d'aide sur du code devraient être posées sur [Stack Overflow](https://stackoverflow.com/) et d'aide sur des maths sur [Math Exchange](https://math.stackexchange.com/).
 - :blush: Soyez gentil(le), civilisé(e) et poli(e) svp ([comme toujours](http://contributor-covenant.org/version/1/4/)).
 - :heart_eyes: Si vous utilisez une *emoji* dans votre question, la réponse devrait arriver plus vite !

### *En anglais ? In English?*
[En Français / In French](https://bitbucket.org/lbesson/ama.fr) ou
[En anglais / In English](https://bitbucket.org/lbesson/ama).

----

### Liens

 - [D'autres AMAs.](https://github.com/sindresorhus/amas)
 - [Qu'est-ce qu'une AMA ?](https://en.wikipedia.org/wiki/Reddit#IAmA_and_AMA)

### :scroll: Licence
Ce (petit) dépôt est publié sous les conditions de la [licence MIT](http://lbesson.mit-license.org/) (fichier [LICENSE.txt](LICENSE.txt), en anglais).

[![Analytics](https://ga-beacon.appspot.com/UA-38514290-17/bitbucket.org/lbesson/ama.fr/README.md?pixel)](https://bitbucket.org/lbesson/ama/)
